# instruction
check package.json for existing scripts

1. clone/unzip
2. install all dependancies
3. create/configure .env
```
git clone https://RyanBours@bitbucket.org/RyanBours/ex.git

npm i

touch .env
#copy the content of .example.env to .env and edit accordingly

```

### build resources

for development:
```
npm run dev
# or use watch
npm run watch
```

for production:
```
npm run prod
```

### run

for development:
```
npm run start
# or
npm run start:watch
```

for production:
```
cross-env NODE_ENV=production npm run start
```

# FAQ
Q: UnhandledPromiseRejectionWarning: Error: Missing delimiting slash between hosts and options

A: missing '/' after port in DB_URL