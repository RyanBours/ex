import express from 'express';

const passport = require('passport');

// middleware
import attachCsrfToken from './middleware/attachCsrfToken'
import attachPaginationHelper from './middleware/AttachPaginationHelper'
import isAuthenticated from './middleware/isAuthenticated'
import attachPugUtil from './middleware/attachPugUtil'

// controllers
import * as Index from './controller/controllerIndex'
import * as Reservering from './controller/controllerReservering'
import * as Bestelling from './controller/controllerBestelling'
import * as Gerecht from'./controller/controllerGerecht'
import * as Overzicht from './controller/controllerOverzicht'
import * as Auth from './controller/controllerAuhtenticate'

const router = express.Router();

// log routing during development
if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === undefined) {
    router.use(function (req, res, next) {
        console.log('%s %s %s %s', req.method, req.path, req.session.csrfSecret)
        next()
    })
}

router.use(attachCsrfToken)
router.use(attachPaginationHelper)
router.use(attachPugUtil)

// Auth routes
router.get('/login', Auth.login)
router.post('/login', passport.authenticate('local', { failureRedirect: '/login'}), Auth.loginPost)
router.get('/register', Auth.register)
router.post('/register', Auth.registerPost)
router.get('/logout', Auth.logout)

router.use(isAuthenticated)

// Routes
router.get(['/', '/index'], Index.index)

// reservering routes
router.get('/reservering', Reservering.index)
router.get('/reservering/create', Reservering.create)
router.post('/reservering/create/', Reservering.post)
router.get('/reservering/edit/:id', Reservering.edit)
router.post('/reservering/edit/:id', Reservering.update)
router.post('/reservering/delete/:id', Reservering.remove)
router.get('/reservering/:id', Reservering.show)

// bestelling routes
router.get('/bestelling/reservering/:id', Bestelling.show)
router.post('/bestelling/add/:id', Bestelling.add)
router.post('/bestelling/remove/:id/:gerecht', Bestelling.remove)
router.get('/bestelling/bon/:id', Bestelling.bon)

// gerechten
router.get('/gerecht', Gerecht.index)
router.post('/gerecht/add', Gerecht.add)

// overzicht routes
router.get('/overzicht', Overzicht.index)

export default router;