const mix = require('laravel-mix');

mix.sass('./resources/scss/app.scss', 'dist/')
    .js('./resources/js/app.js', 'dist/')
    .sourceMaps()
    .setPublicPath('dist')

if (mix.inProduction()) {
    mix.version();
}
    