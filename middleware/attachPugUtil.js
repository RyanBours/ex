import moment from 'moment'
import _ from 'lodash'

export default function attachPugUtil(req, res, next) {
    res.locals.moment = moment
    res.locals._ = _
    next()
}