export default function isAuthenticated(req, res, next) {
    if (!req.user) res.redirect('/login')
    next()
}