import queryString from 'query-string'

export default function attachPaginationFunctions(req, res, next) {
    res.locals.url = req.url
    res.locals.prevPage = (pagination) => {
        return queryString.stringifyUrl({url: req.url, query: {p:pagination.prevPage}})
    }
    res.locals.nextPage = (pagination) => {
        return queryString.stringifyUrl({url: req.url, query: {p:pagination.nextPage}})
    }
    next()
}