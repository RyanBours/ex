import moment from 'moment'

import Model from '../models/index'

// all
export function index(req, res) {
  const pageOptions = {
    limit: 10,
    page: req.query.p > 1 ? req.query.p : 1
  }

  const queryOptions = {
    // TODO: query filters - order by date >=today sort by time closest top 
  }

  Model.Reservering.paginate(queryOptions, pageOptions, (err, reserveringen) => {
    res.render('reservering/index', { reserveringen })
  })
}

// single
export function show(req, res) {
  Model.Reservering.findById(req.params.id, (err, reservering) => {
    if (err || !reservering) {
      // TODO: log error
      req.flash('info', 'unable to find reservering');
      return res.redirect('/reservering')
    }

    res.render('reservering/show', { title: 'show', reservering: reservering })
  })
}

// create - GET create page
export function create(req, res) {
  res.render('reservering/create')
}

// post - create POST route
export function post(req, res) {
  // TODO: validate data

  let reservering = {
    datumtijd: moment(`${req.body.date} ${req.body.time}`),
    tafel: req.body.table,
    naam: req.body.name,
    telefoon: req.body.phone,
    aantal: req.body.amount,
    is_reservering: req.body.is_reserved ? true : false
  }


  Model.Reservering.create(reservering, (err, result) => {
    if (err) return console.log(err)

    // Add empty bestelling on reservering
    Model.Bestelling.create({ reservering: result.id }, (err) => {
      if (err) return console.log(err)
    })
  })

  req.flash('info', 'a reservering has been added');
  res.redirect('/reservering')
}

// edit - GET edit page
export function edit(req, res) {
  Model.Reservering.findById(req.params.id, (err, reservering) => {
    if (err || !reservering) {
      // TODO: log error
      req.flash('info', 'unable to find reservering');
      return res.redirect('/reservering')
    }

    res.render('reservering/edit', { title: 'edit', reservering: reservering })
  })
}

// update - update POST route
export function update(req, res) {
  // TODO: validate data

  let reservering = {
    datumtijd: moment(`${req.body.date} ${req.body.time}`),
    tafel: req.body.table,
    naam: req.body.name,
    telefoon: req.body.phone,
    aantal: req.body.amount,
    is_reservering: req.body.is_reserved ? true : false
  }

  Model.Reservering.findByIdAndUpdate(req.params.id, reservering, (err, reservering) => {
    if (err || !reservering) {
      // TODO: log error
      req.flash('info', 'unable to find reservering');
      return res.redirect('/reservering')
    }

    req.flash('warning', 'reservering has been edited');
    res.redirect('/reservering')
  })
}

// delete - delete POST route
export function remove(req, res) {
  Model.Reservering.findByIdAndDelete(req.params.id, (err, reservering) => {
    if (err || !reservering) {
      // TODO: log error
      req.flash('info', 'unable to find reservering');
      return res.redirect('/reservering')
    }

    Model.Bestelling.findByIdAndRemove(reservering.id)

    req.flash('danger', 'reservering has been removed');
    res.redirect('back')
  })
}