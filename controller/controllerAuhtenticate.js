import model from '../models/index'
const User = model.User;

// GET - login page
export function login(req, res) {
    res.render('auth/login')
}

// POST - login
export function loginPost(req, res) {
    res.redirect('/');
}

// GET - register page
export function register(req, res) {
    res.render('auth/register')
}

// POST - register
export function registerPost(req, res) {
    User.register(new User({ username: req.body.username }), req.body.password, (err, user) => {
        if (err) {
            console.log('error while user register!', err);
            return next(err);
        }
        res.redirect('/')
    })
}

// GET - logout
export function logout(req, res) {
    req.logout();
    res.redirect('/');
}