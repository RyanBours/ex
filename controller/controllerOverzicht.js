import Model from '../models/index'
import _ from 'lodash'

function nest(seq, keys) {
    if (!keys.length) { return seq; }
    let [first, ...rest] = keys;
    return _.mapValues(_.groupBy(seq, first), value => nest(value, rest));
}

export function index(req, res) {
    let queryOptions = {
        
    }
    Model.Bestelling
        .find({})
            .populate('reservering')
            .populate('gerechten.gerecht')
            .exec((err, bestellingen) => {
                let tmp = []
                _(bestellingen)
                    .each((bestelling) => {
                        _(bestelling.gerechten).each((gerecht, v) => {
                            tmp[v] = {
                                reservering: bestelling.reservering,
                                gerecht: gerecht.gerecht,
                                aantal: gerecht.amount
                            }
                        })
                    }
                )
                bestellingen = nest(tmp, ['gerecht.type', 'reservering.id'])
                res.render('overzicht/index', { bestellingen })
            })
}