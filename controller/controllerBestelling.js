import Model from '../models/index'
const mongoose = require('mongoose');

// GET - show bestelling by reservering id
export function show(req, res) {
    Model.Bestelling
        .findOne({ 'reservering': req.params.id })
        .populate('gerechten.gerecht')
        .populate('reservering')
        .exec((err, bestelling) => {
            Model.Gerecht.find({}, (err, gerechten) => {
                if (err) return console.log(err)
                res.render('bestelling/show', { bestelling, gerechten })
            })
        })
}

export function bon(req, res) {
    Model.Bestelling
        .findById(req.params.id)
        .populate('gerechten.gerecht')
        .populate('reservering')
        .exec((err, bestelling) => {
            if (!req.query.hasOwnProperty('pdf'))
                res.render('bestelling/bon', { bestelling })
            else {
                res.renderPDF('bestelling/bon-pdf', { bestelling }, {filename: `bon-${Date.now()}.pdf`})
            }
        })
}

export function add(req, res) {
    // validate
    if (!mongoose.Types.ObjectId.isValid(req.body.gerecht)) {
        req.flash('warning', 'geen gerecht gekozen');
        return res.redirect('back')
    }

    Model.Bestelling.findById(req.params.id, (err, bestelling) => {
        if (err) return console.log(err)

        bestelling.gerechten.push({
            gerecht: mongoose.Types.ObjectId(req.body.gerecht),
            amount: req.body.amount
        })

        bestelling.save((err) => {
            if (err) return console.log(err)
        })
    })

    req.flash('info', 'een gerecht is toegevoegd aan de bestelling')
    res.redirect('back')
}

export function remove(req, res) {
    Model.Bestelling.findById(req.params.id, (err, bestelling) => {
        if (err) return console.log(err)

        bestelling.gerechten.pull(req.params.gerecht)

        bestelling.save((err) => {
            if (err) return console.log(err)
        })
    })
    res.redirect('back')
}