import Model from '../models/index'
import _ from 'lodash'

function nest(seq, keys) {
    if (!keys.length) { return seq; }
    let [first, ...rest] = keys;
    return _.mapValues(_.groupBy(seq, first), value => nest(value, rest));
}


export function index(req, res) {
    Model.Gerecht.find({}, (err, gerechten) => {
        let nested = nest(gerechten, ['type', 'subtype'])
        res.render('gerecht/index', { nested, gerechten })
    })
}

export function add(req, res) {
    // TODO: validation

    let gerecht = {
        type: req.body.type,
        subtype: req.body.subtype,
        naam: req.body.naam,
        omschrijving: req.body.omschrijving,
        prijs: req.body.prijs
    }
    
    Model.Gerecht.create(gerecht, () => {
        req.flash('info', 'een gerecht is toegevoegd')
        res.redirect('back')
    })
}