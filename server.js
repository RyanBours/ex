require('dotenv').config()
import { join } from 'path'

import express from 'express'
import session from 'express-session'

import flash from 'flash'
import csrf from 'csurf'
import bodyParser from 'body-parser'
import pdfRenderer from '@ministryofjustice/express-template-to-pdf'

import passport from 'passport'
import LocalStrategy from 'passport-local'

import mongooseConnection from './db'
import Model from './models/index'

import router from './routes'
import v1 from './api/v1'

const MongoStore = require('connect-mongo')(session);

const port = process.env.PORT || 8080

passport.use(new LocalStrategy(Model.User.authenticate()))
passport.serializeUser(Model.User.serializeUser())
passport.deserializeUser(Model.User.deserializeUser())

express().set('view engine', 'pug')
    .use(session({
        secret: process.env.APP_KEY || 'secret',
        store: new MongoStore({ mongooseConnection: mongooseConnection }),
        resave: false,
        saveUninitialized: true
    }))
    .use(flash())
    .use(pdfRenderer())

    .use(express.static(join(__dirname, 'public')))
    .use(express.static(join(__dirname, 'dist')))

    .use(bodyParser.json())
    .use(bodyParser.urlencoded({
        extended: true
    }))

    .use(passport.initialize())
    .use(passport.session())

    .use('/api/v1', v1)

    .use(csrf())
    .use(router)

    // 404
    .use((req, res, next) => {
        res.status(404);
        res.render('error', {error: 'Page not found', status: '404'});
    })

    // error handler
    .use((err, req, res, next) => {
        // TODO: add logging
        // res.locals.message = err.message;
        // res.locals.error = req.app.get('env') === 'development' ? err : {};


        // render the error page
        res.status(err.status || 500);
        res.render('error', {error: err, status: err.status || 500});
    })

    .listen(port, () => console.log(`Listening on port ${port}!`))
