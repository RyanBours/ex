import { Schema } from 'mongoose';
let SchemaObjectId = Schema.Types.ObjectId;

let SchemaGerecht = new Schema({
    type: String,
    subtype: String,
    naam: String,
    omschrijving: String,
    prijs: Number
}, {
    timestamps: {}
})

SchemaGerecht.pre('save', function (next) {
    next()
})

export default SchemaGerecht;