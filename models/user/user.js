import { Schema } from 'mongoose';
const SchemaObjectId = Schema.Types.ObjectId;
import passportLocalMongoose from 'passport-local-mongoose' 

const SchemaUser = new Schema({}, {
    timestamps: {}
})

SchemaUser.plugin(passportLocalMongoose)

SchemaUser.pre('save', function (next) {
    next()
})

export default SchemaUser;