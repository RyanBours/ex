import { Schema } from 'mongoose';
let SchemaObjectId = Schema.Types.ObjectId;

let SchemaReservering = new Schema({
    datumtijd: Date,
    tafel: Number,
    naam: String,
    telefoon: String,
    aantal: Number,
    is_reservering: Boolean
}, {
    timestamps: {}
})

SchemaReservering.pre('save', function (next) {
    next()
})

export default SchemaReservering;