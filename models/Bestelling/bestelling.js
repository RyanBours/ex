import { Schema } from 'mongoose';
let SchemaObjectId = Schema.Types.ObjectId;

let SchemaBestelling = new Schema({
    reservering: { type: SchemaObjectId, ref: 'reservering' },
    gerechten: [{
        item_id: { type: SchemaObjectId },
        gerecht: { type: SchemaObjectId, ref: 'gerecht' },
        amount: { type: Number, min: 1 }
    }]
}, {
    timestamps: {}
})

SchemaBestelling.pre('save', function (next) {
    next()
})

export default SchemaBestelling;