import mongooseConnection from '../db'

import SchemaUser from '../models/user/user'
import SchemaReservering from '../models/reservering/reservering'
import SchemaGerecht from '../models/Gerecht/gerecht'
import SchemaBestelling from '../models/Bestelling/bestelling'

export default {
    User: mongooseConnection.model('user', SchemaUser),
    Reservering: mongooseConnection.model('reservering', SchemaReservering),
    Gerecht: mongooseConnection.model('gerecht', SchemaGerecht),
    Bestelling: mongooseConnection.model('bestelling', SchemaBestelling)
}