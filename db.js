require('dotenv').config()
import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate-v2'

const mongooseSettings = {
    useCreateIndex: true,
    useNewUrlParser: true,
    dbName: process.env.DB_NAME,
    user: process.env.DB_USER,
    pass: process.env.DB_PASS
}

mongoose.plugin(mongoosePaginate)

const mongooseConnection = mongoose.createConnection(process.env.DB_URL, mongooseSettings);

mongooseConnection.on('error', console.error.bind(console, 'connection error:'));

export default mongooseConnection